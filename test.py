import time

def some_logic_to_test():
    # Your logic here, e.g.,:
    time.sleep(0.1)  # Just an example to simulate some work

def test_example_function(benchmark):
    # Only the logic to test is passed to the benchmark fixture
    result = benchmark(some_logic_to_test)
