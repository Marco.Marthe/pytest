def pytest_addoption(parser):
    parser.addoption("--profile", action="store_true", help="Profile test performance")
